package com.icici.crypto;

import javax.crypto.Cipher;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.crypto.Cipher.*;

public class EncrypterDecrypter {

    public static final String LINE_BREAK               = "\\n";
    public static final String EMPTY_REPLACEMENT        = "";
    public static final String RSA                      = "RSA";
    public static final String BEGIN_PRIVATE_KEY_MSG    = "-----BEGIN RSA PRIVATE KEY-----";
    public static final String END_PRIVATE_KEY_MSG      = "-----END RSA PRIVATE KEY-----";
    public static final String PUBLIC_KEY_PATH          = "src/main/resources/public.txt";
    public static final String PRIVATE_KEY_PATH         = "src/main/resources/private.txt";


    public static String encrypt(String message)  {
        byte[] messageBytes;

        try {
            Cipher cipher = getInstance("RSA/ECB/PKCS1Padding");

            String keyFile = PUBLIC_KEY_PATH;
            InputStream inStream = new FileInputStream(keyFile);
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509Certificate cert = (X509Certificate) cf.generateCertificate(inStream);
            inStream.close();

            messageBytes = message.getBytes();
            RSAPublicKey pubkey = (RSAPublicKey) cert.getPublicKey();

            cipher.init(ENCRYPT_MODE, pubkey);

            byte[] ciphertextBytes = cipher.doFinal(messageBytes);
            String encryptedmsg = Base64.getEncoder().encodeToString(ciphertextBytes);

            return encryptedmsg;

        }catch (Exception e) {
            e.printStackTrace();
            System.out.println("error in encrypt with error message:" + e.getMessage());
        }
        return null;
    }

    public static String decrypt(String msg) throws Exception {

            java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

            Cipher cipher = getInstance("RSA/ECB/PKCS1Padding");
            SecureRandom secureRandom = new SecureRandom();
            String keyFile = PRIVATE_KEY_PATH;
            InputStream inStream = new FileInputStream(keyFile);
            byte[] encKey = new byte[inStream.available()];
            inStream.read(encKey);
            inStream.close();

            String privateKeyAsString = new String(encKey);
            privateKeyAsString =
                    privateKeyAsString
                        .replaceAll(LINE_BREAK, EMPTY_REPLACEMENT)
                        .replace(BEGIN_PRIVATE_KEY_MSG, EMPTY_REPLACEMENT)
                        .replace(END_PRIVATE_KEY_MSG, EMPTY_REPLACEMENT);

            PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyAsString));
            KeyFactory keyFactory = KeyFactory.getInstance(RSA);

            PrivateKey privateKey = keyFactory.generatePrivate(privKeySpec);
            cipher.init(DECRYPT_MODE, privateKey,secureRandom);
            byte[] ciphertextBytes = Base64.getDecoder().decode(msg.getBytes(UTF_8));
            String output = new String(cipher.doFinal(ciphertextBytes));
            return output;
    }



}

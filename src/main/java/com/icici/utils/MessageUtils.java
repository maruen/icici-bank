package com.icici.utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MessageUtils {

    public static  String getDefaultMesssage() {
        Map<String, String> map = new HashMap<>();

        map.put("AGGRID"    , "OTOE0465");
        map.put("AGGRNAME"  , "MCKINSY");
        map.put("CORPID"    , "PRACHICIB1");
        map.put("USERID"    , "USER3");
        map.put("URN"       , "SR212464746");
        map.put("DEBITACC"  , "000451000301");
        map.put("CREDITACC" , "000405001611");
        map.put("IFSC"      , "ICIC0000011");
        map.put("AMOUNT"    , "1");

        JSONObject json= new JSONObject(map);
        String msg = json.toString();
        return msg;
    }

    public static  String getAccountStatementMesssage() {
        Map<String, String> map = new HashMap<>();

        map.put("AGGRID"    , "OTOE0465");
        map.put("AGGRNAME"  , "MCKINSY");
        map.put("CORPID"    , "CIBNEXT");
        map.put("USERID"    , "CIBTESTING6");
        map.put("URN"       , "SR212464746");
        map.put("ACCOUNTNO" , "000405001611");
        map.put("FROMDATE"  , "01-01-2016");
        map.put("TODATE"    , "30-12-2016");

        JSONObject json= new JSONObject(map);
        String msg = json.toString();
        return msg;
    }

    public static  String getRegistrationStatusAPIMessage() {
        Map<String, String> map = new HashMap<>();

        map.put("AGGRNAME"  , "MCKINSY");
        map.put("AGGRID"    , "OTOE0465");
        map.put("CORPID"    , "PRACHICIB1");
        map.put("USERID"    , "USER3");
        map.put("URN"       , "SR212464746");

        JSONObject json= new JSONObject(map);
        String msg = json.toString();
        return msg;
    }

    public static String getMessageToDecrypt() {
        return "ivdom/nfcpjd5pxs0EjHrHnT1Ap0PSA6dWIJ7QHa8fnm4QWQciVkL9L8k62/3BbUZGHvWybWCBvG/Y/fcTAJf7TsYxEZhVGYdsYaNN8ZaMSNvs6TW+nLzXP2vH05VLCoe9oiveC3ltXRaRW2xNRrKT5Q7oKWMZdhcGPLQRNKsYNtIy/gSSTjhcGQWlZZAjkovK/1H6QsrNnbhEFgEOU8A025tDViJ1asepoFXeWHYsVeHgMGqJo1n+wSN7d/4fJLRMTtLGy+MPRGVgYqSquS1IcdrQcqqq3CjD/uIQtlcypy+5jEu3WPxmuL9JA329NtqMNSidBlsNmH9Rq7q5/N+ZfJFE42+v6Ruts1aDYVlnXup97JF9NyC0Oykk7DPm4toSk48s/gc6CeB6TvaLoOjzRTUOUU6v7YWPHK7WCXxCqy96K9zq7QRnh9d1Gx9E692IkzG3VBeiHvcoX0/RvfQv/q3wC4Y26NeeDqqo5cIIIGkaY3sXDNh5YavOsW8XIqB4eUuUHG3wtGZJXqLh/vaABqVjsIaBwLn+6DDBYDF8F4hbvJdC7vYGC/fFPeyQFBwMyL9umLD/UNQVoxdTTgqVxLKWJCgjqXZXTaLBfr1aOX6HkX08IDcTeXZx0FyilbIBKaSaV2U8lROUW+qWK5/s0FaahHAW2SG8RXlJdADK8=";
    }

}

package com.icici.crypto;

import org.junit.Test;

import static com.icici.utils.MessageUtils.getMessageToDecrypt;
import static com.icici.utils.MessageUtils.getRegistrationStatusAPIMessage;
import static java.lang.String.format;

public class TestEncrypterDecrypter {

    @Test
    public void encrypt() {
        System.out.println(format("msgEncrypted -> %s", EncrypterDecrypter.encrypt(getRegistrationStatusAPIMessage())));
    }

    @Test
    public void decrypt() throws Exception {
        System.out.println(format("msgDecrypted -> %s", EncrypterDecrypter.decrypt(getMessageToDecrypt())));
    }
}
